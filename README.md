## Projeto Calculadora de Tintas
  Trata-se de uma aplicação web que ajuda o usuário a calcular a quantidade de tinta necessária para pintar uma sala.
  Essa aplicação considera que a sala é composta de 4 paredes e  permiti que o usuário escolha qual a medida de cada parede e quantas janelas e portas possuem cada parede.
  Com base na quantidade necessária o sistema aponta tamanhos de lata de tinta que o usuário deve comprar, sempre priorizando as latas maiores. Ex: se o usuário precisa de 19 litros, ele deve sugerir 1 lata de 18L + 2 latas de 0,5L


## Instruções para rodar a Aplicação

1. Clone o repositório
  * `git@gitlab.com:lucasrm1804/calculadora-de-tinta.git`
  * Entre na pasta do repositório que você acabou de clonar:
    * `cd calculadora-de-tinta`

2. Instale as dependências e inicialize o projeto
  * Instale as dependências:
    * `npm install`
  * Inicialize o projeto:
    * `npm start` 

