export const verifyWallHeight = (height, door) => {
  const magicNumber = 2.2;
  if (typeof height !== 'number' || height <= 0) {
    return false;
  }
  if (door > 0) {
    if (height < magicNumber) {
      return false;
    }
  }
  return true;
};

export const verifyWallWidth = (width) => {
  if (typeof width !== 'number' || width <= 0) {
    return false;
  }
  return true;
};

export const verifyArea = (height, width, door, window) => {
  const area = height * width;
  const doorAndWindow = door * window;
  if (area < 1 || area > 50) {
    return false;
  }
  if (doorAndWindow / area > 0.5) {
    return false;
  }
  return true;
};

export const verifyData = (height, width, door, window) => {
  return (
    verifyWallWidth(width) &&
    verifyWallHeight(height, door) &&
    verifyArea(height, width, door, window)
  );
};
