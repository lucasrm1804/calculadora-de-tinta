export default function calculatorLata(areaTotal, func1, func2, func3, func4) {
  const litros = areaTotal / 5;

  const resto1 = litros % 18;
  const resto2 = resto1 % 3.6;
  const resto3 = resto2 % 2.5;

  litros >= 18 ? func1(Math.floor(litros / 18)) : func1(0);
  resto1 >= 3.6 ? func2(Math.floor(resto1 / 3.6)) : func2(0);
  resto2 >= 2.5 ? func3(Math.floor(resto2 / 2.5)) : func3(0);
  resto3 >= 0 ? func4(Math.ceil(resto3 / 0.5)) : func4(0);
}
