import React from 'react';
import { Route, Switch } from 'react-router-dom';
import NotFound from '../Pages/NotFound';
import InicialPage from '../Pages/InicialPage/InicialPage';

export default function Routes() {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={InicialPage} />

        <Route path="*" component={NotFound} />
      </Switch>
    </div>
  );
}
