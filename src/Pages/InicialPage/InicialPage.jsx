import React from 'react';
import Header from '../../Componets/Header/Header';
import Footer from '../../Componets/Footer/Footer';
import Body from '../../Componets/Body/Body';

export default function NotFound() {
  return (
    <div>
      <Header label="Calculadora de Tinta" />

      <Body />

      <Footer />
    </div>
  );
}
