import React, { useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import AppContext from './appContext';

function Provider({ children }) {
  const [parede1, setParede1] = useState(0);
  const [parede2, setParede2] = useState(0);
  const [parede3, setParede3] = useState(0);
  const [parede4, setParede4] = useState(0);

  const contextValue = useMemo(
    () => ({
      parede1,
      setParede1,
      parede2,
      setParede2,
      parede3,
      setParede3,
      parede4,
      setParede4,
    }),
    [parede1, parede2, parede3, parede4]
  );

  return (
    <AppContext.Provider value={contextValue}>
      {children}
    </AppContext.Provider>
  );
}

Provider.propTypes = {
  children: PropTypes.string.isRequired,
};

export default Provider;
