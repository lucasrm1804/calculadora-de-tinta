import React, { useContext, useState } from 'react';
import appContext from '../../context/appContext';
import FormInput from '../FormInput/FormInput';
import ButtonForm from '../ButtonForm/ButtonForm';
import { verifyData } from '../../Tools/verifications'

export default function FormWall() {
  const [altura, setAltura] = useState(0);
  const [largura, setLargura] = useState(0);
  const [portas, setPortas] = useState(0);
  const [janelas, setJanelas] = useState(0);
  const [index, setIndex] = useState(1);
  const { setParede1, setParede2, setParede3, setParede4 } = useContext(appContext);

  const doorArea = 2.00 * 1.20;
  const windowArea =  0.80 * 1.90;

  const onClick = (height, width, door, window) => {
    if(verifyData(height, width, (door *doorArea), (window * windowArea))) {
      switch (index) {
        case 1:
          setParede1((height * width) - ((door * doorArea) + (window * windowArea)));
          setIndex( index + 1 );
          break;
        case 2:
          setParede2((height * width) - ((door * doorArea) + (window * windowArea)));
          setIndex( index + 1 );
          break;
        case 3:
          setParede3((height * width) - ((door * doorArea) + (window * windowArea)));
          setIndex( index + 1 );
          break;  
        case 4:
          setParede4((height * width) - ((door * doorArea) + (window * windowArea)));
          setIndex(index +1);
          break;
            }
              setAltura(0); setLargura(0); setPortas(0); setJanelas(0);
            }
          }
            
            return (
    <div className="card-wall">
      <FormInput
        label="Altura"
        placeholder="Informe a altura em metros"
        value={altura}
        step="0.1"
        onChange={setAltura}
      />
      <FormInput
        label="Largura"
        placeholder="Informe a largura em metros"
        value={largura}
        step="0.1"
        onChange={setLargura}

      />
      <FormInput
        label="Portas"
        placeholder="Informe a quantidade de portas"
        value={portas}
        step="1"
        onChange={setPortas}
      />

      <FormInput
        label="Janelas"
        placeholder="Informe a quantidade de janelas"
        value={janelas}
        step="1"
        onChange={setJanelas}
      />{index <=4
      ?
      <ButtonForm 
      disabled={!verifyData(altura, largura, (portas *doorArea), (janelas * windowArea))}
      text={index <=4?`Parede ${index}`:``}
      handleClick={() => onClick(altura, largura, portas, janelas)}
      />
      : null}
    </div>
  );
}
