import React from 'react';
import PropTypes from 'prop-types';
import './FormInput.css';

export default function FormInput(props) {
  const { label, placeholder, value, onChange, step } = props;

  return (
    <div>
      <form>
        <div className="container-input">
          <label>{label}</label>
          <input
            onChange={({ target }) => {
              onChange(Number(target.value));
            }}
            value={
              label === 'Portas' || label === 'Janelas'
                ? Math.floor(value)
                : value
            }
            type="number"
            placeholder={placeholder}
            step={step}
            min="0"
          />
          <span className="instructions">{placeholder}</span>
        </div>
      </form>
    </div>
  );
}

FormInput.propTypes = {
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  step: PropTypes.string.isRequired,
};
