import React from 'react';
import './Footer.css';

export default function Footer() {
  return (
    <footer>
      <h3>Todos os direitos Reservados, Lucas</h3>

      <nav>
        <a
          href="https://www.linkedin.com/in/lucas-ribeiro-monteiro-dev/"
          target="_blank"
          rel="noreferrer"
        >
          <img className="link" src="/linkedin.png" alt="linkedin" />
        </a>

        <a
          href="https://github.com/lucasrm1804"
          target="_blank"
          rel="noreferrer"
        >
          <img className="link" src="/github.png" alt="github" />
        </a>
      </nav>
    </footer>
  );
}
