import React from 'react';
import FormWall from '../FormWall/FormWall';
import PaintCalc from '../PaintCalc/PaintCalc';
import WallResultCard from '../WallResultCard/WallResultCard';
import './Body.css';

export default function Body() {
  return (
    <>
      <div className="text">
        <div>
          <span className="subtitle">
            Utilize nossa ferramenta e calcule a quantidade de latas de tinta
            necessária para pintar um ambiente.
          </span>
        </div>
        <hr />
        <hr />
        <div>
          <span className="orientation">
            Nos campos abaixo informe em metros a altura e a largura da área
            onde será pintada. Informe tambem a quantidade de portas ou janelas.
          </span>
        </div>
      </div>
      <div className="cards-container">
        <FormWall />
        <WallResultCard />
      </div>
      <div className="card-result">
        <PaintCalc />
      </div>
    </>
  );
}
