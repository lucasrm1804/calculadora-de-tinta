import React from 'react';
import PropTypes from 'prop-types';
import './ButtonForm.css';

export default function ButtonForm(props) {
  const { text, handleClick, disabled } = props;
  return (
    <button
      disabled={disabled}
      type="button"
      onClick={() => handleClick()}
      className="blue-Button"
    >
      {text}
    </button>
  );
}

ButtonForm.propTypes = {
  text: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
};
