import React, { useContext, useEffect, useState } from 'react';
import appContext from '../../context/appContext';
import calculatorLata from '../../Tools/calculations';
import './PaintCalc.css';

export default function PaintCalc() {
  const { parede1, parede2, parede3, parede4 } = useContext(appContext);
  const areaTotal = (parede1 + parede2 + parede3 + parede4).toFixed(2);
  const [lataUm, setLataUm] = useState(0);
  const [lataDois, setLataDois] = useState(0);
  const [lataTres, setLataTres] = useState(0);
  const [lataQuatro, setLataQuatro] = useState(0);

  useEffect(() => {
    calculatorLata(
      areaTotal,
      setLataUm,
      setLataDois,
      setLataTres,
      setLataQuatro
    );
  }, [areaTotal]);

  return (
    <div>
      {areaTotal !== 0 && (
        <div>
          <div>{`Total da area a ser pintada: ${areaTotal} m²`}</div>
          <li>
            Sugerimos a compra de:
            {lataUm !== 0 && (
              <ol>
                {lataUm > 1
                  ? `${lataUm} Latas de 18L`
                  : `${lataUm} Lata de 18L`}
              </ol>
            )}
            {lataDois !== 0 && (
              <ol>
                {lataDois > 1
                  ? `${lataDois} Latas de 3,6L`
                  : `${lataDois} Lata de 3,6L`}
              </ol>
            )}
            {lataTres !== 0 && (
              <ol>
                {lataTres > 1
                  ? `${lataTres} Latas de 2,5L`
                  : `${lataTres} Lata de 2,5L`}
              </ol>
            )}
            {lataQuatro !== 0 && (
              <ol>
                {lataQuatro > 1
                  ? `${lataQuatro} Latas de 0,5L`
                  : `${lataQuatro} Lata de 0,5L`}
              </ol>
            )}
          </li>
        </div>
      )}
    </div>
  );
}
