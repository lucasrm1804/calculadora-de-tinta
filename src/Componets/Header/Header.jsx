import React from 'react';
import PropTypes from 'prop-types';
import './Header.css';

export default function Header(props) {
  const { label } = props;

  return (
    <div className="header">
      <h1>{label}</h1>
    </div>
  );
}

Header.propTypes = {
  label: PropTypes.string.isRequired,
};
