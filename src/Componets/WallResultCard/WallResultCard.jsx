import React, { useContext } from 'react';
import appContext from '../../context/appContext';
import WallResultInput from '../WallResultInput/WallResultInput';
import './WallResultCard.css';

export default function WallResultCard() {
  const { parede1, parede2, parede3, parede4 } = useContext(appContext);

  return (
    <div className="card-container">
      <div>
        {parede1 !== 0 && <WallResultInput value={parede1} order={1} />}

        {parede3 !== 0 && <WallResultInput value={parede3} order={3} />}
      </div>

      <div>
        {parede2 !== 0 && <WallResultInput value={parede2} order={2} />}

        {parede4 !== 0 && <WallResultInput value={parede4} order={4} />}
      </div>
    </div>
  );
}
