import React from 'react';
import PropTypes from 'prop-types';
import './WallResultInput.css';

export default function WallResultInput(props) {
  const { value, order } = props;

  return (
    <p className="wall-Input">
      {`Area total da parede ${order}:`}
      <span className="span-value">{` ${value}m²` }</span>
    </p>
  );
}

WallResultInput.propTypes = {
  value: PropTypes.number.isRequired,
  order: PropTypes.number.isRequired
};
